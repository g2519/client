import { helpers } from 'vuelidate/lib/validators';
const alphaNumWithSpaces = helpers.regex('alphaNumWithSpaces', /^[a-z0-9_ ]*$/i)

export {
    alphaNumWithSpaces
}
