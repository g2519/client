import Vue from 'vue'
import VueRouter from 'vue-router'
import Project from '../views/Project.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/', redirect: { name: 'Project' }
  },
  {
    path: '/projects',
    name: 'Project',
    component: Project,
    // children: [
    //   {
    //     path: '/:id',
    //     name: 'Project',
    //     component: Project,
    //   },
    // ]
  },
  {
    path: '/employees',
    name: 'Employee',
    component: () => import('../views/Employee.vue')
  },
]

const router = new VueRouter({
  routes
})

export default router
